<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Libraries;

use Illuminate\Support\Facades\App;
use App\Http\Libraries\Tools;
use App\Http\Libraries\Session;
use App\Model\Tbl_permissions;
use App\Model\Tbl_classes;

/**
 * Description of Config
 *
 * @author root
 */
class Config {

    //put your code here


    public static function initConfig() {
        return array(
            'config' => array(
                '_app_core_id' => env('APP_CORE_ID'), //max and min length is 32 only number
                '_app_uuid' => Session::_get('_uuid'),
                '_app_platform' => env('APP_PLATFORM'),
                '_config_modulName' => Tools::getRoutes('modul'),
                '_config_className' => str_replace('Controller', '', Tools::getRoutes('controller')),
                '_config_methodName' => Tools::getRoutes('action'),
                '_config_layoutPath' => 'Layouts.',
                '_config_full_url' => \Request::fullUrl(),
                '_config_base_url' => url('/'),
                '_frontend_login_url' => route('login', ['locale' => App::getLocale()]),
                '_frontend_logout_url' => route('logout', ['locale' => App::getLocale()]),
                '_backend_login_url' => route('backend-login', ['locale' => App::getLocale()]),
                '_backend_logout_url' => route('backend-logout', ['locale' => App::getLocale()]),
                '_config_redirect_frontend_url' => url('/') . env('FRONTEND_LOGIN_REDIRECT'),
                '_config_redirect_cms_url' => url('/') . env('BACKEND_LOGIN_REDIRECT'),
                '_config_lib_url' => url('/') . '/Lib/',
                '_config_img_url' => url('/') . '/Img/',
                '_current_segment' => Tools::getUriSegment2(\Request::fullUrl(), false),
                '_redirect_auth' => url('/users'),
                '_app_url_curr' => env('APP_URL'),
                '_app_url_prod' => env('APP_URL_PROD'),
                '_app_url_rc' => env('APP_URL_RC')
            )
        );
    }

    public static function initPath($obj) {
        $config = Config::initConfig()['config'];
        $path_view_js = '';
        $path_view_html = '';
        if (count(explode('/', $config['_current_segment'])) == 1) {
            $get_route_name = \Request::route()->getName();
            $Tbl_permissions = new Tbl_permissions();
            $permission = $Tbl_permissions->find('first', array('fields' => 'all', 'table_name' => 'tbl_permissions', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'a.route' => '="' . $get_route_name . '"'))));
            if ($permission != null && $permission->is_generated_view == 1) {
                $Tbl_classes = new Tbl_classes();
                $class = $Tbl_classes->find('first', array('fields' => 'all', 'table_name' => 'tbl_classes', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'a.id' => '="' . $permission->class_id . '"'))));
                if ($class != null) {
                    $class = str_replace('/', '.', $class->full_path);
                    $path_view_js = $class . '.' . $permission->method . '_js';
                    $path_view_html = $class . '.' . $permission->method . '_html';
                } else {
                    $path_view_js = $class . '.' . $permission->method . '_js';
                    $path_view_html = $class . '.' . $permission->method . '_html';
                }
            }
        } else {
            $path_view_js = $config['_config_modulName'] . '.' . $config['_current_segment'] . '_js';
            $path_view_html = $config['_config_modulName'] . '.' . $config['_current_segment'] . '_html';
        }
        return array(
            'path' => array(
                '_path_compressPath' => 'Includes/compress',
                '_path_cssPath' => 'Includes/css/',
                '_path_filesPath' => 'Includes/files',
                '_path_imagesPath' => 'Includes/images',
                '_path_jsPath' => 'Includes/js',
                '_path_videosPath' => 'Includes/videos',
                '_path_libPath' => 'Lib',
                '_path_contentPath' => 'Components.content',
                '_path_globalJsPath' => 'Ajax.global_js',
                '_path_content_path_js' => 'Pages.' . $path_view_js,
                '_path_content_path_html' => 'Pages.' . $path_view_html
            )
        );
    }

}
