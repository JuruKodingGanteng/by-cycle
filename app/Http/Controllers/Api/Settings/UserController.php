<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Controller;
use App\Http\Libraries\Auth;
use App\Http\Libraries\Tools;
use Request;
use App\Model\Tbl_user_tokens;
use App\Model\Tbl_users;
use App\Model\Tbl_user_groups;
use App\Model\Tbl_group_permissions;
use App\Model\Tbl_groups;

/**
 * Description of UserController
 *
 * @author root
 */
class UserController extends Controller {

    //put your code here


    public function generate_token_fe() {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            if (count($post) == 1) {
                $data = array(
                    'deviceid' => $post['deviceid']
                );
                $validate = Auth::generate_global_token($data);
                debug($validate);
            } else {
                $data = array(
                    'userid' => $post['username'],
                    'password' => base64_decode($post['password']),
                    'deviceid' => $post['device_id']
                );
                //validate userid and password
                if (Tools::getValidEmail($data['userid'])) {
                    $option_validate = array(
                        'email' => $data['userid'],
                        'password' => $data['password']
                    );
                } else {
                    $option_validate = array(
                        'userid' => $data['userid'],
                        'password' => $data['password']
                    );
                }
                $validate = json_decode(Auth::validate_password($option_validate));
            }
            if ($validate->status == 200) {
                return json_encode(array('status' => 200, 'message' => 'success', 'data' => array('token' => $validate->data->token)));
            } else {
                return json_encode(array('status' => 201, 'message' => 'wrong password', 'data' => null));
            }
        } else {
            return response()->json(['status' => 201, 'message' => 'you send empty params', 'data' => null]);
        }
    }

    public function generate_token_be() {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            if (count($post) == 1) {
                $data = array(
                    'deviceid' => $post['deviceid']
                );
                $validate = Auth::generate_global_token($data);
                debug($validate);
            } else {
                $data = array(
                    'userid' => $post['username'],
                    'password' => base64_decode($post['password']),
                    'deviceid' => $post['device_id']
                );
                //validate userid and password
                if (Tools::getValidEmail($data['userid'])) {
                    $option_validate = array(
                        'email' => $data['userid'],
                        'password' => $data['password']
                    );
                } else {
                    $option_validate = array(
                        'userid' => $data['userid'],
                        'password' => $data['password']
                    );
                }
                $validate = json_decode(Auth::validate_password($option_validate));
            }
            if ($validate->status == 200) {
                return json_encode(array('status' => 200, 'message' => 'success', 'data' => array('token' => $validate->data->token)));
            } else {
                return json_encode(array('status' => 201, 'message' => 'wrong password', 'data' => null));
            }
        } else {
            return response()->json(['status' => 201, 'message' => 'you send empty params', 'data' => null]);
        }
    }

    public function drop_user_session() {
        $token = Request::header('token');
        $Tbl_user_tokens = new Tbl_user_tokens();
        $user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        if (isset($user_token) && !empty($user_token)) {
            Auth::session_data_clear($user_token);
            return json_encode(array('status' => 200, 'message' => 'Successfully delete user session', 'data' => null));
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed, token invalid', 'data' => null));
        }
    }

    public function is_logged_in() {
        $token = Request::header('token');
        $Tbl_user_tokens = new Tbl_user_tokens();
        $user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        if (isset($user_token) && !empty($user_token)) {
            return json_encode(array('status' => 200, 'message' => 'youre in logged in session', 'data' => null));
        } else {
            return json_encode(array('status' => 201, 'message' => 'youre not in logged in session', 'data' => null));
        }
    }

    public function get_user_details() {
        $token = Request::header('token');
        $Tbl_user_tokens = new Tbl_user_tokens();
        $user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        if (isset($user_token) && !empty($user_token)) {
            $user = Tbl_users::find('first', array('fields' => 'all', 'table_name' => 'tbl_users', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'id' => '"' . $user_token->user_id . '"'))));
            $group_user = Tbl_user_groups::find('first', array('fields' => 'all', 'table_name' => 'tbl_user_groups', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'a.user_id' => '"' . $user_token->user_id . '"'))));
            $group = Tbl_groups::find('first', array('fields' => 'all', 'table_name' => 'tbl_groups', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'a.id' => '"' . $group_user->group_id . '"'))));
            $res = array(
                'id' => $user->id,
                'username' => $user->username,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'group_id' => $group->id,
                'group_name' => $group->name,
                'is_active' => $user->is_active,
                'created_date' => $user->created_date,
            );
            return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $res));
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed retrieving data', 'data' => null));
        }
    }

    public function get_user_permissions() {
        $token = Request::header('token');
        $Tbl_user_tokens = new Tbl_user_tokens();
        $user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        if (isset($user_token) && !empty($user_token)) {
            $user_group = Tbl_user_groups::find('first', array('fields' => 'all', 'table_name' => 'tbl_user_groups', 'conditions' => array('where' => array('a.is_active' => '= "1"', 'a.user_id' => '"' . $user_token->user_id . '"'))));
            $res = Tbl_group_permissions::do_query("SELECT * FROM `tbl_group_permissions` a LEFT JOIN tbl_permissions b ON a.permission_id = b.id WHERE a.group_id = $user_group->group_id ORDER BY b.module ASC");
            return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $res));
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed retrieving data', 'data' => null));
        }
    }

    public function verify_password() {
        $token = Request::header('token');
        $Tbl_user_tokens = new Tbl_user_tokens();
        $user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        if (isset($user_token) && !empty($user_token)) {
            return json_encode(array('status' => 200, 'message' => 'youre in logged in session', 'data' => null));
        } else {
            return json_encode(array('status' => 201, 'message' => 'youre not in logged in session', 'data' => null));
        }
    }

}
