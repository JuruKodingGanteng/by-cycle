<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Controller;
use App\Http\Libraries\Auth;
use App\Http\Libraries\Tools;
use Request;
use App\Model\Tbl_user_tokens;
use App\Model\Tbl_d_email_subscribes;

/**
 * Description of NewsletterController
 *
 * @author root
 */
class NewsletterController extends Controller {

    public function __construct() {
        parent::__construct();
    }

    public function insert() {
        //$token = Request::header('token');
        //$Tbl_user_tokens = new Tbl_user_tokens();
        //$user_token = $Tbl_user_tokens->find('first', array('fields' => 'all', 'table_name' => 'tbl_user_tokens', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.token_generated' => '="' . $token . '"'))));
        //if (isset($user_token) && !empty($user_token)) {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            $insert_data = [
                'email' => $post['email'],
                'first_name' => '-',
                'last_name' => '-',
                'facebook' => '-',
                'twitter' => '-',
                'instagram' => '-',
                'linkedin' => '-',
                'is_newsletter_subs' => 1,
                'is_active' => 1,
                "created_by" => 0,
                "created_date" => Tools::getDateNow()
            ];
            $Tbl_d_email_subscribes = new Tbl_d_email_subscribes();
            $res = $Tbl_d_email_subscribes->insert_return_id($insert_data);
            if (isset($res) && !empty($res) && $res != null) {
                return json_encode(array('status' => 200, 'message' => 'Successfully insert data.', 'data' => ['id' => $res]));
            } else {
                return json_encode(array('status' => 201, 'message' => 'Failed insert data, or data not empty', 'data' => null));
            }
        }
        //} else {
        //    return json_encode(array('status' => 202, 'message' => 'Token is miss matched or expired', 'data' => null));
        //}
    }

    public function validate_exist_email() {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            $Tbl_d_email_subscribes = new Tbl_d_email_subscribes();
            $child = $Tbl_d_email_subscribes->find('first', array('fields' => 'all', 'table_name' => 'tbl_d_email_subscribes', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.email' => '="' . $post['email'] . '"'))));
            if (isset($child) && !empty($child) && $child != null) {
                return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => null));
            } else {
                return json_encode(array('status' => 201, 'message' => 'Failed retrieving data, or data not found', 'data' => null));
            }
        }
    }

}
