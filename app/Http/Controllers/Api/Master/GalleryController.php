<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Controller;
use Request;
use App\Model\Tbl_e_gallery_products;
use App\Model\Tbl_e_galleries;
use App\Model\Tbl_b_product_items;
use App\Model\Tbl_b_products;

/**
 * Description of GalleryController
 *
 * @author root
 */
class GalleryController extends Controller {

    //put your code here

    public function __construct() {
        parent::__construct();
    }

    //put your code here
    public function get_list() {
        $Tbl_e_gallery_products = new Tbl_e_gallery_products();
        $galleries = $Tbl_e_gallery_products->find('all', array('fields' => 'all', 'table_name' => 'tbl_e_gallery_products', 'conditions' => array('where' => array('a.is_active' => '="1"'))));
        if (isset($galleries) && !empty($galleries) && $galleries != null) {
            $arr_data = array();
            foreach ($galleries AS $key => $val) {
                $Tbl_e_product_items = new Tbl_b_product_items;
                $product_items = $Tbl_e_product_items->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_items', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '="' . $val->product_item_id . '"'))));

                $Tbl_b_products = new Tbl_b_products();
                $product = $Tbl_b_products->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_products', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_items->product_id))));

                $Tbl_e_galleries = new Tbl_e_galleries;
                $galerries = $Tbl_e_galleries->find('first', array('fields' => 'all', 'table_name' => 'tbl_e_galleries', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '="' . $val->gallery_id . '"'))));

                $arr_data[] = array(
                    'product_gallery_product' => $val,
                    'product_item' => $product_items,
                    'product_details' => $product,
                    'product_galleries' => $galerries
                );
            }
            return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $arr_data));
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed retrieving data, or data not found', 'data' => null));
        }
    }

    public function find() {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $Tbl_menus = new Tbl_menus();
            $child = $Tbl_menus->find('first', array('fields' => 'all', 'table_name' => 'tbl_menus', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '="' . $id . '"'))));
            if (isset($child) && !empty($child) && $child != null) {
                if ($child->parent_id != 0) {
                    $parents = $Tbl_menus->find('first', array('fields' => 'all', 'table_name' => 'tbl_menus', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '="' . $child->parent_id . '"'))));
                    $child->parent_name = $parents->name;
                }
                return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $child));
            } else {
                return json_encode(array('status' => 201, 'message' => 'Failed retrieving data, or data not found', 'data' => null));
            }
        }
    }

}
