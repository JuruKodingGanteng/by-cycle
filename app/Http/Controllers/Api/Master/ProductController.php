<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\Controller;
use App\Http\Libraries\Tools;
use App\Model\Tbl_user_tokens;
use App\Model\Tbl_b_products;
use App\Model\Tbl_b_product_items;
use App\Model\Tbl_b_product_categories;
use App\Model\Tbl_b_product_descriptions;
use App\Model\Tbl_b_product_geometry;
use App\Model\Tbl_b_product_specifications;
use App\Model\Tbl_b_product_types;
use App\Model\Tbl_b_product_videos;
use App\Model\Tbl_b_product_colors;
use App\Model\Tbl_b_product_sizes;
use App\Model\Tbl_b_product_images;
use Request;

/**
 * Description of ProductController
 *
 * @author root
 */
class ProductController extends Controller {

    //put your code here

    public function get_list() {
        $Tbl_b_product_items = new Tbl_b_product_items();
        $product_items = $Tbl_b_product_items->find('all', array('fields' => 'all', 'table_name' => 'tbl_b_product_items', 'conditions' => array('where' => array('a.is_active' => '="1"'))));
        $arr_val = array();
        if (isset($product_items) && !empty($product_items) && $product_items != null) {
            foreach ($product_items AS $key => $product_item_list) {
                //Tbl_b_products
                $Tbl_b_products = new Tbl_b_products();
                $product = $Tbl_b_products->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_products', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->product_id))));

                $Tbl_b_product_categories = new Tbl_b_product_categories();
                $product_category = $Tbl_b_product_categories->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_categories', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->category_id))));

                $Tbl_b_product_descriptions = new Tbl_b_product_descriptions();
                $product_desc = $Tbl_b_product_descriptions->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_descriptions', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->description_id))));

                $Tbl_b_product_geometry = new Tbl_b_product_geometry();
                $product_geometry = $Tbl_b_product_geometry->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_geometry', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->geometry_id))));

                $Tbl_b_product_specifications = new Tbl_b_product_specifications();
                $product_specification = $Tbl_b_product_specifications->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_specifications', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->specification_id))));

                $Tbl_b_product_types = new Tbl_b_product_types();
                $product_type = $Tbl_b_product_types->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_types', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->type_id))));

                $Tbl_b_product_videos = new Tbl_b_product_videos();
                $product_video = $Tbl_b_product_videos->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_videos', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->video_id))));

                $Tbl_b_product_colors = new Tbl_b_product_colors();
                $product_color = $Tbl_b_product_colors->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_colors', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->color_id))));

                $Tbl_b_product_sizes = new Tbl_b_product_sizes();
                $product_size = $Tbl_b_product_sizes->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_sizes', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->size_id))));

                $Tbl_b_product_images = new Tbl_b_product_images();
                $product_images = $Tbl_b_product_images->find('all', array('fields' => 'all', 'table_name' => 'tbl_b_product_images', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.product_item_id' => '=' . $product_item_list->id))));
                $arr_product_image = array();
                if ($product_images) {
                    foreach ($product_images AS $k => $img) {
                        $arr_product_image[] = array(
                            'id' => $img->id,
                            'path' => $this->_config_img_url . 'products/item/' . $img->path_1
                        );
                    }
                }
                $arr_val[] = array(
                    'item' => array(
                        'id' => $product_item_list->id,
                        'sku_code' => $product_item_list->sku_code,
                        'stock_in' => $product_item_list->stock_in,
                        'stock_out' => $product_item_list->stock_out,
                        'stock_end' => $product_item_list->stock_end,
                        'price_online' => number_format($product_item_list->price_online, 0, ',', '.'),
                        'price_online_masking' => number_format((int) ($product_item_list->price_online + ($product_item_list->price_online * 0.25)), 0, ',', '.'), //,
                        'price_offline' => number_format($product_item_list->price_offline, 0, ',', '.'), //,
                        'price_starter' => number_format($product_item_list->price_starter, 0, ',', '.'), //,
                    ),
                    'detail' => $product,
                    'images' => $arr_product_image,
                    'category' => $product_category,
                    'description' => $product_desc,
                    'geometry' => $product_geometry,
                    'specification' => $product_specification,
                    'type' => $product_type,
                    'video' => $product_video,
                    'color' => $product_color,
                    'size' => $product_size
                );
            }
            return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $arr_val));
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed retrieving data', 'data' => null));
        }
    }

    public function find() {
        $post = Request::post();
        if (isset($post) && !empty($post)) {
            $id = base64_decode($post['id']);
            $Tbl_b_product_items = new Tbl_b_product_items();
            $product_items = $Tbl_b_product_items->find('all', array('fields' => 'all', 'table_name' => 'tbl_b_product_items', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $id))));
            $arr_val = array();
            if (isset($product_items) && !empty($product_items) && $product_items != null) {
                foreach ($product_items AS $key => $product_item_list) {
                    //Tbl_b_products
                    $Tbl_b_products = new Tbl_b_products();
                    $product = $Tbl_b_products->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_products', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->product_id))));

                    $Tbl_b_product_categories = new Tbl_b_product_categories();
                    $product_category = $Tbl_b_product_categories->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_categories', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->category_id))));

                    $Tbl_b_product_descriptions = new Tbl_b_product_descriptions();
                    $product_desc = $Tbl_b_product_descriptions->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_descriptions', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->description_id))));

                    $Tbl_b_product_geometry = new Tbl_b_product_geometry();
                    $product_geometry = $Tbl_b_product_geometry->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_geometry', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->geometry_id))));

                    $Tbl_b_product_specifications = new Tbl_b_product_specifications();
                    $product_specification = $Tbl_b_product_specifications->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_specifications', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->specification_id))));

                    $Tbl_b_product_types = new Tbl_b_product_types();
                    $product_type = $Tbl_b_product_types->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_types', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->type_id))));

                    $Tbl_b_product_videos = new Tbl_b_product_videos();
                    $product_video = $Tbl_b_product_videos->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_videos', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->video_id))));

                    $Tbl_b_product_colors = new Tbl_b_product_colors();
                    $product_color = $Tbl_b_product_colors->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_colors', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->color_id))));

                    $Tbl_b_product_sizes = new Tbl_b_product_sizes();
                    $product_size = $Tbl_b_product_sizes->find('first', array('fields' => 'all', 'table_name' => 'tbl_b_product_sizes', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.id' => '=' . $product_item_list->size_id))));

                    $Tbl_b_product_images = new Tbl_b_product_images();
                    $product_images = $Tbl_b_product_images->find('all', array('fields' => 'all', 'table_name' => 'tbl_b_product_images', 'conditions' => array('where' => array('a.is_active' => '="1"', 'a.product_item_id' => '=' . $product_item_list->id))));
                    $arr_product_image = array();
                    if ($product_images) {
                        foreach ($product_images AS $k => $img) {
                            $arr_product_image[] = array(
                                'id' => $img->id,
                                'path' => $this->_config_img_url . 'products/item/' . $img->path_1
                            );
                        }
                    }
                    $arr_val[] = array(
                        'item' => array(
                            'id' => $product_item_list->id,
                            'sku_code' => $product_item_list->sku_code,
                            'stock_in' => $product_item_list->stock_in,
                            'stock_out' => $product_item_list->stock_out,
                            'stock_end' => $product_item_list->stock_end,
                            'price_online' => number_format($product_item_list->price_online, 0, ',', '.'),
                            'price_online_masking' => number_format((int) ($product_item_list->price_online + ($product_item_list->price_online * 0.25)), 0, ',', '.'), //,
                            'price_offline' => number_format($product_item_list->price_offline, 0, ',', '.'), //,
                            'price_starter' => number_format($product_item_list->price_starter, 0, ',', '.'), //,
                        ),
                        'detail' => $product,
                        'images' => $arr_product_image,
                        'category' => $product_category,
                        'description' => $product_desc,
                        'geometry' => $product_geometry,
                        'specification' => $product_specification,
                        'type' => $product_type,
                        'video' => $product_video,
                        'color' => $product_color,
                        'size' => $product_size
                    );
                }
                return json_encode(array('status' => 200, 'message' => 'Successfully retrieving data.', 'data' => $arr_val[0]));
            } else {
                return json_encode(array('status' => 201, 'message' => 'Failed retrieving data', 'data' => null));
            }
        } else {
            return json_encode(array('status' => 201, 'message' => 'Failed retrieving data', 'data' => null));
        }
    }

}
