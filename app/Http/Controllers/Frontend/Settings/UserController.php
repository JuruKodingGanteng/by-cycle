<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Frontend\Settings;

use App\Http\Controllers\Controller;
use App\Http\Libraries\Session;
use App\Http\Libraries\Auth;

/**
 * Description of UserController
 *
 * @author root
 */
class UserController extends Controller {

    //put your code here

    public function index() {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
         $load_css = array(
            'slick/slick/slick.css',
            'slick/slick/slick-theme.css',
        );
        $this->load_css($load_css);
        $load_js = array(
            'slick/slick/slick.js',
        );
        $this->load_js($load_js);
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

    public function by_category($lang = 'en', $key = null) {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
        $data['title_header'] = 'View Product by category list ' . $key;
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

    public function by_menu_bikes($lang = 'en', $key = null) {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
        $data['title_header'] = 'View Bike list';
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

    public function by_menu_dealer($lang = 'en', $key = null) {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
        $data['title_header'] = 'View our dealer list location';
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

    public function by_menu_sparepart($lang = 'en', $key = null) {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
        $data['title_header'] = 'View our sparepart stock';
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

    public function by_menu_contact($lang = 'en', $key = null) {
        $data['title_for_layout'] = 'Welcome to bycyle.com';
        $data['title_header'] = 'Contact Us';
        return view($this->_config_layoutPath . 'Autima.index', $data);
    }

}
