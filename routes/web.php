<?php

use Illuminate\Support\Facades\Route;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', 'Frontend\Settings\UserController@index')->name('homepage');
Route::get('/{locale}', 'Frontend\Settings\UserController@index')->name('homepage');

Route::get('/{locale}/login', 'Auth\Frontend\UserController@login', function ($locale) {
    App::setLocale($locale);
})->name('login');
Route::get('/{locale}/logout', 'Auth\Frontend\UserController@logout', function ($locale) {
    App::setLocale($locale);
})->name('logout');

Route::get('/{locale}/cms/login', 'Auth\Backend\UserController@login', function ($locale) {
    App::setLocale($locale);
})->name('backend-login');
Route::get('/{locale}/cms/logout', 'Auth\Backend\UserController@logout', function ($locale) {
    App::setLocale($locale);
})->name('backend-logout');



Route::post('/auth/save-token-fe', 'Auth\Frontend\UserController@save_token')->name('save-token-fe');
Route::post('/auth/save-token-be', 'Auth\Backend\UserController@save_token')->name('save-token-be');
/*
 * Front end routes start here
 */
Route::get('/{locale}/product-category/{slug}', 'Frontend\Settings\UserController@by_category', function ($locale) {
    App::setLocale($locale);
})->name('by-category');
Route::get('/{locale}/bikes', 'Frontend\Settings\UserController@by_menu_bikes', function ($locale) {
    App::setLocale($locale);
})->name('by-menu-bikes');
Route::get('/{locale}/dealer', 'Frontend\Settings\UserController@by_menu_dealer', function ($locale) {
    App::setLocale($locale);
})->name('by-menu-dealer');
Route::get('/{locale}/sparepart', 'Frontend\Settings\UserController@by_menu_sparepart', function ($locale) {
    App::setLocale($locale);
})->name('by-menu-sparepart');
Route::get('/{locale}/contact-us', 'Frontend\Settings\UserController@by_menu_contact', function ($locale) {
    App::setLocale($locale);
})->name('by-menu-contact-us');

Route::get('/{locale}/product-detail/{title}/{id}', 'Frontend\Settings\ProductController@index', function ($locale) {
    App::setLocale($locale);
})->name('product-detail');
//Route::get('/{locale}/product-category/{slug}', 'Frontend\Settings\UserController@by_category')->name('by-category');
//Route::get('/{locale}/bikes', 'Frontend\Settings\UserController@by_menu_bikes')->name('by-menu-bikes');
//Route::get('/{locale}/dealer', 'Frontend\Settings\UserController@by_menu_dealer')->name('by-menu-dealer');
//Route::get('/{locale}/sparepart', 'Frontend\Settings\UserController@by_menu_sparepart')->name('by-menu-sparepart');
//Route::get('/{locale}/contact-us', 'Frontend\Settings\UserController@by_menu_contact')->name('by-menu-contact-us');

/*
 * Front end routes end here
 */

Route::group(['prefix' => 'cms'], function () {
    /*
     * login & logout routes end here
     */

    Route::get('/dashboard', 'Backend\Settings\UserController@dashboard')->name('dashboard');
    Route::get('/api-docs', 'Backend\Settings\ApiDocsController@view')->name('api-docs');
    Route::get('/settings', 'Backend\Settings\SettingsController@list')->name('settings');

    /*
     * backend crud
     */

    //menu
    Route::get('/prefferences/menu/view', 'Backend\Prefferences\MenuController@view')->name('view-menu');
    Route::post('/prefferences/menu/get-list', 'Backend\Prefferences\MenuController@get_list')->name('get-list-menu');
    Route::post('/prefferences/menu/get-data', 'Backend\Master\CountryController@get_data')->name('get-data-menu');
    Route::put('/prefferences/menu/insert', 'Backend\Prefferences\MenuController@insert')->name('insert-menu');
    Route::post('/prefferences/menu/update', 'Backend\Prefferences\MenuController@update')->name('update-menu');
    Route::post('/prefferences/menu/update-status', 'Backend\Prefferences\MenuController@update_status')->name('update-status-menu');
    Route::post('/prefferences/menu/remove', 'Backend\Prefferences\MenuController@remove')->name('remove-menu');
    Route::delete('/prefferences/menu/delete', 'Backend\Prefferences\MenuController@delete')->name('delete-menu');

    //permission
    Route::get('/settings/permission/view', 'Backend\Settings\PermissionController@view')->name('view');
    Route::post('/settings/permission/get-list', 'Backend\Settings\PermissionController@get_list')->name('get-list');
    Route::post('/settings/permission/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::put('/settings/permission/insert', 'Backend\Settings\PermissionController@insert')->name('insert');
    Route::post('/settings/permission/update', 'Backend\Settings\PermissionController@update')->name('update');
    Route::post('/settings/permission/update-status', 'Backend\Settings\PermissionController@update_status')->name('update_status');
    Route::post('/settings/permission/remove', 'Backend\Settings\PermissionController@remove')->name('remove');
    Route::delete('/settings/permission/delete', 'Backend\Settings\PermissionController@delete')->name('delete');

    //country
    Route::get('/location/country/view', 'Backend\Master\CountryController@view')->name('view');
    Route::post('/location/country/get-list', 'Backend\Master\CountryController@get_list')->name('get-list');
    Route::put('/location/country/insert', 'Backend\Master\CountryController@insert')->name('insert');
    Route::post('/location/country/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::post('/location/country/update', 'Backend\Master\CountryController@update')->name('update');
    Route::post('/location/country/update-status', 'Backend\Master\CountryController@update_status')->name('update_status');
    Route::post('/location/country/remove', 'Backend\Master\CountryController@remove')->name('remove');
    Route::delete('/location/country/delete', 'Backend\Master\CountryController@delete')->name('delete');

    //province
    Route::get('/location/province/view', 'Backend\Master\ProvinceController@view')->name('view');
    Route::post('/location/province/get-list', 'Backend\Master\ProvinceController@get_list')->name('get-list');
    Route::post('/location/province/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::put('/location/province/insert', 'Backend\Master\ProvinceController@insert')->name('insert');
    Route::post('/location/province/update', 'Backend\Master\ProvinceController@update')->name('update');
    Route::post('/location/province/update-status', 'Backend\Master\CountryController@update_status')->name('update_status');
    Route::post('/location/province/remove', 'Backend\Master\CountryController@remove')->name('remove');
    Route::delete('/location/province/delete', 'Backend\Master\ProvinceController@delete')->name('delete');

    //district
    Route::get('/location/district/view', 'Backend\Master\DistrictController@view')->name('view');
    Route::post('/location/district/get-list', 'Backend\Master\DistrictController@get_list')->name('get-list');
    Route::put('/location/district/insert', 'Backend\Master\DistrictController@insert')->name('insert');
    Route::post('/location/district/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::post('/location/district/update', 'Backend\Master\DistrictController@update')->name('update');
    Route::post('/location/district/update-status', 'Backend\Master\CountryController@update_status')->name('update_status');
    Route::post('/location/district/remove', 'Backend\Master\CountryController@remove')->name('remove');
    Route::delete('/location/district/delete', 'Backend\Master\DistrictController@delete')->name('delete');

    //sub-district
    Route::get('/location/sub-district/view', 'Backend\Master\SubDistrictController@view')->name('view');
    Route::post('/location/sub-district/get-list', 'Backend\Master\SubDistrictController@get_list')->name('get-list');
    Route::post('/location/sub-district/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::put('/location/sub-district/insert', 'Backend\Master\SubDistrictController@insert')->name('insert');
    Route::post('/location/sub-district/update', 'Backend\Master\SubDistrictController@update')->name('update');
    Route::post('/location/sub-district/update-status', 'Backend\Master\CountryController@update_status')->name('update_status');
    Route::post('/location/sub-district/remove', 'Backend\Master\CountryController@remove')->name('remove');
    Route::delete('/location/sub-district/delete', 'Backend\Master\SubDistrictController@delete')->name('delete');

    //area
    Route::get('/location/area/view', 'Backend\Master\AreaController@view')->name('view');
    Route::post('/location/area/get-list', 'Backend\Master\AreaController@get_list')->name('get-list');
    Route::post('/location/area/get-data', 'Backend\Master\CountryController@get_data')->name('get-data');
    Route::put('/location/area/insert', 'Backend\Master\AreaController@insert')->name('insert');
    Route::post('/location/area/update', 'Backend\Master\AreaController@update')->name('update');
    Route::post('/location/area/update-status', 'Backend\Master\CountryController@update_status')->name('update_status');
    Route::post('/location/area/remove', 'Backend\Master\CountryController@remove')->name('remove');
    Route::delete('/location/area/delete', 'Backend\Master\AreaController@delete')->name('delete');
});
