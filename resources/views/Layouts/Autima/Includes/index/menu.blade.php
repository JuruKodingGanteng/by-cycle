<div class="Offcanvas_menu">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="canvas_open">
                    <span>MENU</span>
                    <a href="javascript:void(0)"><i class="ion-navicon"></i></a>
                </div>
                <div class="Offcanvas_menu_wrapper">

                    <div class="canvas_close">
                        <a href="#"><i class="ion-android-close"></i></a>
                    </div>

                    <div class="top_right text-right">
                        <ul>
                            <li class="top_links"><a href="#"><i class="ion-android-person"></i> My Account<i class="ion-ios-arrow-down"></i></a>
                                <ul class="dropdown_links">
                                    <li><a href="checkout.html">Checkout </a></li>
                                    <li><a href="my-account.html">My Account </a></li>
                                    <li><a href="cart.html">Shopping Cart</a></li>
                                </ul>
                            </li>
                            <li class="language"><a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/logo/language.png'; ?>" alt="">en-id<i class="ion-ios-arrow-down"></i></a>
                                <ul class="dropdown_language">
                                        <li><a href="#"><i class="fas fa-flag"></i><!--<img src="<?php //echo $_config_lib_url . 'autima/assets/img/logo/language.png';         ?>" alt="">--> English</a></li>
                                        <li><a href="#"><i class="fas fa-flag"></i><!--<img src="<?php //echo $_config_lib_url . 'autima/assets/img/logo/language2.png';         ?>" alt="">--> Indonesian</a></li>
                                </ul>
                            </li>
                            <li class="currency"><a href="#">IDR<i class="ion-ios-arrow-down"></i></a>
                                <ul class="dropdown_currency">
                                    <li><a href="#">IDR – Indonesian Rupiah</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="search-container">
                        <form action="#">
                            <div class="search_box">
                                <input placeholder="Search entire store here ..." type="text">
                                <button type="submit"><i class="ion-ios-search-strong"></i></button>
                            </div>
                        </form>
                    </div>
                    <div id="menu" class="text-left ">
                        <ul class="offcanvas_main_menu">
                            @include('Elements.Autima.helper.frontend_menu') 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>