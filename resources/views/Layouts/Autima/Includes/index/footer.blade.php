<footer class="footer_widgets">
    <div class="container">
        <div class="footer_top">
            <div class="row">
                <div class="col-lg-4 col-md-6">
                    <div class="widgets_container contact_us">
                        <div class="footer_logo">
                            <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/logo/logo.png';?>" alt=""></a>
                        </div>
                        <div class="footer_contact">
                            <p>We are a team of designers and developers that
                                create high quality Magento, Prestashop, Opencart...</p>
                            <p><span>Address</span> 4710-4890 Breckinridge St, UK Burlington, VT 05401</p>
                            <p><span>Need Help?</span>Call: <a href="tel:1-800-345-6789">1-800-345-6789</a></p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widgets_container widget_menu">
                        <h3>@lang('homepage.footer.information')</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="about.html">@lang('homepage.footer.about_us')</a></li>
                                <li><a href="#">@lang('homepage.footer.delivery_information')</a></li>
                                <li><a href="privacy-policy.html">@lang('homepage.footer.privacy_policy')</a></li>
                                <li><a href="coming-soon.html">@lang('homepage.footer.coming_soon')</a></li>
                                <li><a href="#">@lang('homepage.footer.term_cond')</a></li>
                                <li><a href="#">@lang('homepage.footer.return')</a></li>
                                <li><a href="#">@lang('homepage.footer.gift_certificates')</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-6 col-sm-6">
                    <div class="widgets_container widget_menu">
                        <h3>@lang('homepage.footer.extra')</h3>
                        <div class="footer_menu">
                            <ul>
                                <li><a href="#">@lang('homepage.footer.return')</a></li>
                                <li><a href="#">@lang('homepage.footer.order_history')</a></li>
                                <li><a href="#">@lang('homepage.footer.newsletter')</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="widgets_container">
                        <h3>@lang('homepage.footer.newsletter_subscribe')</h3>
                        <p>@lang('homepage.footer.we_never_share_your_email')</p>
                        <div class="subscribe_form">
                            <form id="mc-form" class="mc-form footer-newsletter">
                                <input id="mc-email" type="email" autocomplete="off" placeholder="Enter you email address here..." />
                                <button id="mc-submit">@lang('homepage.footer.subscribe')</button>
                            </form>
                            <!-- mailchimp-alerts Start -->
                            <div class="mailchimp-alerts text-centre">
                                <div class="mailchimp-submitting"></div><!-- mailchimp-submitting end -->
                                <div class="mailchimp-success"></div><!-- mailchimp-success end -->
                                <div class="mailchimp-error"></div><!-- mailchimp-error end -->
                            </div><!-- mailchimp-alerts end -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer_bottom">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="copyright_area">
                        <p>@lang('homepage.footer.copyright') &copy; 2019 <a href="#">Orenoproject</a> All Right Reserved.</p>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="footer_payment text-right">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/icon/payment.png';?>" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>