<!-- CSS ========================= -->
<!--bootstrap min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/bootstrap.min.css'; ?>">
<!--owl carousel min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/owl.carousel.min.css'; ?>">
<!--slick min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/slick.css'; ?>">
<!--magnific popup min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/magnific-popup.css'; ?>">
<!--font awesome css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/font.awesome.css'; ?>">
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'fontawesome/css/all.css'; ?>"> 
<!--ionicons min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/ionicons.min.css'; ?>">
<!--animate css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/animate.css'; ?>">
<!--jquery ui min css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/jquery-ui.min.css'; ?>">
<!--slinky menu css-->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/slinky.menu.css'; ?>">
<!-- Plugins CSS -->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/plugins.css'; ?>">
<!-- Main Style CSS -->
<link rel="stylesheet" href="<?php echo $_config_lib_url . 'autima/assets/css/style.css'; ?>">
<!-- include optional styles start here -->
<link href="<?php echo $_config_lib_url . 'toastr/build/toastr.min.css'; ?>" rel="stylesheet"/>
<!--modernizr min js here-->
<script src="<?php echo $_config_lib_url . 'autima/assets/js/vendor/modernizr-3.7.1.min.js'; ?>"></script>

<style>
    #loading-lottie{
        display: none;
        z-index: 999999999;
        position: fixed;
        top: 50%;
        left: 50%;
    }
</style>