<section class="slider_section mb-50">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-12">
                <div class="categories_menu">
                    <div class="categories_title">
                        <h2 class="categori_toggle">@lang('homepage.categories.browse_categories')</h2>
                    </div>
                    <div class="categories_menu_toggle">
                        <ul>
                            <?php if (isset($sidebar_menu) && !empty($sidebar_menu)): ?>
                                <?php foreach ($sidebar_menu AS $keyword => $values): ?>
                                    <li class="menu_item_children categorie_list">
                                        <a href="<?php echo $values->uri; ?>"><?php echo $values->name; ?></a>
                                    </li>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-9 col-md-12">
                <div class="slider_area owl-carousel" id="galleryHomePage"></div>
            </div>
        </div>
</section>
