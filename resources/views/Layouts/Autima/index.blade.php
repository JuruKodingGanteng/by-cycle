<!doctype html>
<html class="no-js" lang="en">

    <head>
        <title><?php echo isset($title_for_layout) ? $title_for_layout : ''; ?></title>
        @include('Layouts.Autima.Includes.index.include_meta') 
        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="<?php echo $_config_lib_url . 'autima/assets/img/favicon.ico' ?>">
        @include('Layouts.Autima.Includes.index.include_css') 
    </head>

    <body>
        <div id="loading-lottie" ><div id="loading"></div></div>
        <!-- Main Wrapper Start -->
        @include('Layouts.Autima.Includes.index.header') 
        <!--Offcanvas menu area start-->
        <div class="off_canvars_overlay"></div>
        <!@include('Layouts.Autima.Includes.index.menu') 
        <!--Offcanvas menu area end-->
        <?php if ($_config_className === 'User' && $_config_methodName == 'index'): ?>
            <!--slider area start-->
            @include('Layouts.Autima.Includes.index.categories') 
            <!--slider area end-->
        <?php endif; ?>
        <!--product area start-->
        <?php if ($_path_contentPath): ?>
            @include("{$_path_contentPath}")
        <?php endif; ?>
        <!--product area end-->
        @include('Elements.Autima.custom_product') 
        <!--custom product end-->
        <!--call to action start-->
        @include('Elements.Autima.social_media') 
        <!--call to action end-->
        <!--footer area start-->
        @include('Layouts.Autima.Includes.index.footer') 
        <!--footer area end-->
        <!--news letter popup start-->
        @include('Elements.modal.Autima.index') 
        <!-- JS ============================================ -->
        @include('Layouts.Autima.Includes.index.include_js') 
    </body>
</html>