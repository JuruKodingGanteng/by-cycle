<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<script src="dist/js/demo.js"></script>
<script src="dist/js/pages/dashboard3.js"></script>

<script src="<?php echo $_config_lib_url . 'autima/assets/js/main.js' ?>" type="text/javascript"></script>
<script src="<?php echo $_config_base_url . '/Includes/js/base64.js'; ?>" type="text/javascript"></script>
<script src="<?php echo $_config_base_url . '/Includes/js/dateFormat.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url . 'toastr/build/toastr.min.js'; ?>"></script>
<script src="<?php echo $_config_lib_url . 'bodymovin/5.6.5/lottie.js'; ?>"></script>

<?php if (isset($_config_base_url) && !empty($_config_base_url)): ?>
    var _config_base_url = '<?php echo $_config_base_url; ?>';
<?php else: ?>
    var _config_base_url = '';
<?php endif; ?>
<?php if (isset($_config_lib_url) && !empty($_config_lib_url)): ?>
    var _config_lib_url = '<?php echo $_config_lib_url; ?>';
<?php else: ?>
    var _config_lib_url = '';
<?php endif; ?>
<?php if (isset($_config_img_url) && !empty($_config_img_url)): ?>
    var _config_img_url = '<?php echo $_config_img_url; ?>';
<?php else: ?>
    var _config_img_url = '';
<?php endif; ?>

<?php if (isset($_is_logged_in) && !empty($_is_logged_in)) : ?>
    var _is_logged_in = '<?php echo $_is_logged_in; ?>';
<?php else: ?>
    var _is_logged_in = '';
<?php endif; ?>
</script>

<!-- load system variable to js function start here -->
<?php if (isset($js_var) && !empty($js_var)): ?>
    <script>
    <?php foreach ($js_var AS $key => $values): ?>
        <?php foreach ($values AS $k => $v): ?>
            <?php if ($k == 'app'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'config'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'path'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'options'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'appUri'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>

        <?php endforeach; ?>
    <?php endforeach; ?>
    </script>
<?php endif; ?>
<!-- load system variable to js function start here -->

<!-- load variable to js function from controller start here -->
<?php if (isset($load_ajax_var) && !empty($load_ajax_var)) : ?>
    <?php foreach ($load_ajax_var AS $key => $values): ?>
        <script> var <?php echo $values['key']; ?> = '<?php echo $values['val'] ?>';</script>
    <?php endforeach; ?>
<?php endif; ?>
<!-- load variable to js function from controller end here -->

<!-- load js lib / class / library from controller start here -->
<?php if (isset($load_js) && !empty($load_js)) : ?>
    <?php foreach ($load_js AS $key => $values) : ?>
        <script src="<?php echo $_config_lib_url . $values ?>" type="text/javascript"></script>
    <?php endforeach; ?>
<?php endif; ?>
<!-- load js lib / class / library from controller end here -->

<!-- load global js lib for every controller start here -->
<?php if ($_path_globalJsPath): ?>
    @include("{$_path_globalJsPath}")
<?php endif; ?>
<!-- load global js lib for every controller end here -->

<!-- load specified js lib for a view start here -->
<?php if ($_path_content_path_js): ?>
    @include("{$_path_content_path_js}")
<?php endif; ?>