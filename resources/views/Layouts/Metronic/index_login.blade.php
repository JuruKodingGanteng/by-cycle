<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo isset($title_for_layout) ? $title_for_layout : ''; ?></title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        @include('Layouts.Metronic.Includes.index_login.include_meta') 
        @include('Layouts.Metronic.Includes.index_login.include_css') 
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="login">
        <div id="loading-lottie" ><div id="loading"></div></div>
        @include('Layouts.Metronic.Includes.index_login.logo') 
        <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
        <div class="menu-toggler sidebar-toggler"></div>
        <!-- END SIDEBAR TOGGLER BUTTON -->
        <?php if ($_path_contentPath): ?>
            @include("{$_path_contentPath}")
        <?php endif; ?>
        <!-- END LOGIN -->
        @include('Layouts.Metronic.Includes.index_login.footer') 
        @include('Layouts.Metronic.Includes.index_login.include_js') 
    </body>
    <!-- END BODY -->
</html>