<!-- BEGIN JAVASCRIPTS(Load javascripts at bottom, this will reduce page load time) -->
<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php //echo $_config_lib_url           ?>/metronic/assets/global/plugins/respond.min.js"></script>
<script src="<?php //echo $_config_lib_url           ?>/metronic/assets/global/plugins/excanvas.min.js"></script> 
<![endif]-->
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN PAGE LEVEL PLUGINS -->
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/backstretch/jquery.backstretch.min.js" type="text/javascript"></script>
<script type="text/javascript" src="<?php echo $_config_lib_url ?>metronic/assets/global/plugins/select2/select2.min.js"></script>
<script src="<?php echo $_config_base_url . '/Includes/js/base64.js'; ?>" type="text/javascript"></script>
<script src="<?php echo $_config_base_url . '/Includes/js/dateFormat.min.js'; ?>" type="text/javascript"></script>
<script src="<?php echo $_config_lib_url . 'toastr/build/toastr.min.js'; ?>"></script>
<script src="<?php echo $_config_lib_url . 'bodymovin/5.6.5/lottie.js'; ?>"></script>
<!-- END PAGE LEVEL PLUGINS -->
<script>
<?php if (isset($_app_uuid) && !empty($_app_uuid)): ?>
        var _app_uuid = '<?php echo $_app_uuid; ?>';
<?php else: ?>
        var _app_uuid = '';
<?php endif; ?>
<?php if (isset($_token) && !empty($_token)): ?>
        var _token = '<?php echo $_token; ?>';
<?php else: ?>
        var _token = '';
<?php endif; ?>
<?php if (isset($_config_base_url) && !empty($_config_base_url)): ?>
        var _config_base_url = '<?php echo $_config_base_url; ?>';
<?php else: ?>
        var _config_base_url = '';
<?php endif; ?>
<?php if (isset($_config_lib_url) && !empty($_config_lib_url)): ?>
        var _config_lib_url = '<?php echo $_config_lib_url; ?>';
<?php else: ?>
        var _config_lib_url = '';
<?php endif; ?>
<?php if (isset($_config_img_url) && !empty($_config_img_url)): ?>
        var _config_img_url = '<?php echo $_config_img_url; ?>';
<?php else: ?>
        var _config_img_url = '';
<?php endif; ?>
<?php if (isset($_lang) && !empty($_lang)) : ?>
        var _lang = '<?php echo $_lang; ?>';
<?php else: ?>
        var _lang = '';
<?php endif; ?>
<?php if (isset($_config_redirect_frontend_url) && !empty($_config_redirect_frontend_url)): ?>
        var _config_redirect_frontend_url = '<?php echo $_config_redirect_frontend_url; ?>';
<?php else: ?>
        var _config_redirect_frontend_url = '';
<?php endif; ?>
<?php if (isset($_config_redirect_backend_url) && !empty($_config_redirect_backend_url)): ?>
        var _config_redirect_backend_url = '<?php echo $_config_redirect_backend_url; ?>';
<?php else: ?>
        var _config_redirect_backend_url = '';
<?php endif; ?>
<?php if (isset($_frontend_login_url) && !empty($_frontend_login_url)): ?>
        var _frontend_login_url = '<?php echo $_frontend_login_url; ?>';
<?php else: ?>
        var _frontend_login_url = '';
<?php endif; ?>

<?php if (isset($_frontend_logout_url) && !empty($_frontend_logout_url)): ?>
        var _frontend_logout_url = '<?php echo $_frontend_logout_url; ?>';
<?php else: ?>
        var _frontend_logout_url = '';
<?php endif; ?>

<?php if (isset($_backend_login_url) && !empty($_backend_login_url)): ?>
        var _backend_login_url = '<?php echo $_backend_login_url; ?>';
<?php else: ?>
        var _backend_login_url = '';
<?php endif; ?>

<?php if (isset($_backend_logout_url) && !empty($_backend_logout_url)): ?>
        var _backend_logout_url = '<?php echo $_backend_logout_url; ?>';
<?php else: ?>
        var _backend_logout_url = '';
<?php endif; ?>
</script>

<!-- load system variable to js function start here -->
<?php if (isset($js_var) && !empty($js_var)): ?>
    <script>
    <?php foreach ($js_var AS $key => $values): ?>
        <?php foreach ($values AS $k => $v): ?>
            <?php if ($k == 'app'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'config'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'path'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'options'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>
            <?php if ($k == 'appUri'): ?>
                <?php foreach ($v AS $j => $n): ?>
                        var <?php echo $j; ?> = '<?php echo $n ?>';
                <?php endforeach; ?>
            <?php endif; ?>

        <?php endforeach; ?>
    <?php endforeach; ?>
    </script>
<?php endif; ?>
<!-- load system variable to js function start here -->

<!-- load variable to js function from controller start here -->
<?php if (isset($load_ajax_var) && !empty($load_ajax_var)) : ?>
    <?php foreach ($load_ajax_var AS $key => $values): ?>
        <script> var <?php echo $values['key']; ?> = '<?php echo $values['val'] ?>';</script>
    <?php endforeach; ?>
<?php endif; ?>
<!-- load variable to js function from controller end here -->

<!-- load js lib / class / library from controller start here -->
<?php if (isset($load_js) && !empty($load_js)) : ?>
    <?php foreach ($load_js AS $key => $values) : ?>
        <script src="<?php echo $_config_public_url . $values ?>"></script>
    <?php endforeach; ?>
<?php endif; ?>
<!-- load js lib / class / library from controller end here -->

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo $_config_lib_url ?>metronic/assets/global/scripts/metronic.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
        // init background slide images
        $.backstretch([
            "<?php echo $_config_lib_url ?>metronic/assets/admin/pages/media/bg/1.jpg",
            "<?php echo $_config_lib_url ?>metronic/assets/admin/pages/media/bg/2.jpg",
            "<?php echo $_config_lib_url ?>metronic/assets/admin/pages/media/bg/3.jpg",
            "<?php echo $_config_lib_url ?>metronic/assets/admin/pages/media/bg/4.jpg"
        ], {
            fade: 1000,
            duration: 8000
        }
        );
    });
</script>
<!-- END JAVASCRIPTS -->

<!-- load global js lib for every controller start here -->
<?php if ($_path_globalJsPath): ?>
    @include("{$_path_globalJsPath}")
<?php endif; ?>
<!-- load global js lib for every controller end here -->

<!-- load specified js lib for a view start here -->
<?php if ($_path_content_path_js): ?>
    @include("{$_path_content_path_js}")
<?php endif; ?>