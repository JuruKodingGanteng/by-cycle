<div class="brand_area mb-42">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span> <strong>Brand</strong>Collection</span></h2>
                </div>
                <div class="brand_container owl-carousel">
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand.png';?>" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand1.png';?>" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand2.png';?>" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand3.png';?>" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand4.png';?>" alt=""></a>
                    </div>
                    <div class="single_brand">
                        <a href="#"><img src="<?php echo $_config_lib_url . 'autima/assets/img/brand/brand2.png';?>" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>