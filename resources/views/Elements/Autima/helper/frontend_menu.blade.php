<?php if (isset($frontend_menu) && !empty($frontend_menu)): ?>
    <?php foreach ($frontend_menu AS $keyword => $values): ?>
        <li>
            <a href="<?php echo $_config_base_url . '/' . $values['href']; ?>"><?php echo $values['text']; ?><?php if (isset($values['nodes']) && !empty($values['nodes'])): ?><i class="fa fa-angle-down"></i><?php endif; ?></a>
            <?php if (isset($values['nodes']) && !empty($values['nodes'])): ?>
                <ul class="sub_menu">
                    <?php foreach ($values['nodes'] AS $key => $val): ?>
                        <li><a href="<?php echo $_config_base_url . '/' . $val['href']; ?>"><?php echo $val['text']; ?></a></li>
                    <?php endforeach; ?>
                </ul>
            <?php endif; ?>
        </li>
    <?php endforeach; ?>
<?php endif; ?>