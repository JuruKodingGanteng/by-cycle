<style>
    .modal_tab_button ul li a {
        padding: 0;
        border: 1px solid transparent!important;
        margin: 0 2px;
    }
</style>
<section class="product_area mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="product_name">
                    <h3 class="link_product"><a href="#">Product name</a></h3>
                    <p class="manufacture_product"><a href="#">Category name</a></p>
                </div>
                <div class="product_thumb">
                    <a class="primary_img" href="' + link_product + '"><img src="' + path + '" alt="" style="min-height:180px"></a>
                    <a class="secondary_img" href="' + link_product + '"><img src="' + path + '" alt="" style="min-height:180px"></a>
                    <div class="label_product">
                        <span class="label_sale">-57%</span>
                    </div>
                    <div class="action_links">
                        <ul>
                            <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" data-id="' + data[i].item.id + '" title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>
                            <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>
                            <li class="compare"><a href="compare.html" title="compare"><span class="lnr lnr-sync"></span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="product_content">
                    <div class="product_ratings">
                        <ul>
                            <li><a href="#"><i class="ion-star"></i></a></li>
                            <li><a href="#"><i class="ion-star"></i></a></li>
                            <li><a href="#"><i class="ion-star"></i></a></li>
                            <li><a href="#"><i class="ion-star"></i></a></li>
                            <li><a href="#"><i class="ion-star"></i></a></li>
                        </ul>
                    </div>
                    <div class="product_footer d-flex align-items-center">
                        <div class="price_box">
                            <span class="regular_price">IDR </span>
                        </div>
                        <a href="<?php echo $_config_base_url . '/' . $_lang . '/add-product-to-cart/'; ?>" title="add to cart"><span class="lnr lnr-cart"></span></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>