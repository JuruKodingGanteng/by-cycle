<script>
    var el_carousel = '#carousel';
    var page_carousel = 1;
    var page_per_show_carousel = 10;
    var fnSetMenuTop3 = function () {
        var uri = _config_base_url + '/api/fetch/menu';
        var type = 'POST';
        var formdata = {
            module_id: 2,
            logged: 1,
            module_name: 'Frontend'
        };
        var response = fnAjaxSend(formdata, uri, type, {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}, false);
        if (response.responseJSON.status === 200) {
            console.log(response);
        }
        return false;
    };
    var fnGetCarousel = function (el, page, length, type) {
        if (type === 'add') {
            page++;
        }
        var screenWidth = screen.width;
        var imgWidthGlobal = 1900;
        if (screenWidth <= 1366) {
            imgWidthGlobal = 1024;
        } else if (screenWidth <= 1400) {
            imgWidthGlobal = 1200;
        }

    };
    var retrieveProductDate = function () {
        var uri = _config_base_url + '/api/fetch/product-list';
        var type = 'POST';
        var formdata = {
            page: 1,
            offset: 0,
            perpage: 20
        };
        var response = fnAjaxSend(formdata, uri, type, {}, false);
        var str = '';
        if (response.responseJSON.status === 200) {
            var data = response.responseJSON.data;
            for (var i = 0; i < data.length; i++) {
                var path = _config_img_url + 'products/item/default-sepeda.png';
                if (data[i].images[0]) {
                    path = data[i].images[0].path;
                }
                var link_product = _config_base_url + '/' + _lang + '/product-detail/' + str_slug(data[i].detail.name) + '/' + data[i].item.id;
                str = str + '<div class="single_product">';
                str = str + '    <div class="product_name">';
                str = str + '        <h3><a href="' + link_product + '">' + data[i].detail.name + '</a></h3>';
                str = str + '        <p class="manufacture_product"><a href="#">' + data[i].category.name + '</a></p>';
                str = str + '    </div>';
                str = str + '    <div class="product_thumb">';
                str = str + '        <a class="primary_img" href="' + link_product + '"><img src="' + path + '" alt="" style="min-height:180px"></a>';
                str = str + '        <a class="secondary_img" href="' + link_product + '"><img src="' + path + '" alt="" style="min-height:180px"></a>';
                str = str + '        <div class="label_product">';
                str = str + '            <span class="label_sale">-57%</span>';
                str = str + '        </div>';
                str = str + '        <div class="action_links">';
                str = str + '            <ul>';
                str = str + '                <li class="quick_button"><a href="#" data-toggle="modal" data-target="#modal_box" data-id="' + data[i].item.id + '" title="quick view"> <span class="lnr lnr-magnifier"></span></a></li>';
                str = str + '                <li class="wishlist"><a href="wishlist.html" title="Add to Wishlist"><span class="lnr lnr-heart"></span></a></li>';
                str = str + '                <li class="compare"><a href="compare.html" title="compare"><span class="lnr lnr-sync"></span></a></li>';
                str = str + '            </ul>';
                str = str + '        </div>';
                str = str + '    </div>';
                str = str + '    <div class="product_content">';
                str = str + '       <div class="product_ratings">';
                str = str + '            <ul>';
                str = str + '                <li><a href="#"><i class="ion-star"></i></a></li>';
                str = str + '                <li><a href="#"><i class="ion-star"></i></a></li>';
                str = str + '                <li><a href="#"><i class="ion-star"></i></a></li>';
                str = str + '                <li><a href="#"><i class="ion-star"></i></a></li>';
                str = str + '                <li><a href="#"><i class="ion-star"></i></a></li>';
                str = str + '            </ul>';
                str = str + '        </div>';
                str = str + '        <div class="product_footer d-flex align-items-center">';
                str = str + '            <div class="price_box">';
                str = str + '                <span class="regular_price">IDR ' + data[i].item.price_online + '</span>';
                str = str + '           </div>';
                str = str + '            <div class="add_to_cart">';
                str = str + '                <a href="' + _config_base_url + '/' + _lang + '/add-product-to-cart/' + str_slug(data[i].detail.name) + '/' + data[i].item.id + '" title="add to cart"><span class="lnr lnr-cart"></span></a>';
                str = str + '            </div>';
                str = str + '        </div>';
                str = str + '    </div>';
                str = str + '</div>';
            }
        }
        return str;
    };

    var fnGetProductList = function () {
        var str = retrieveProductDate();
        $('#productHomePage').append(str);
    };

    var fnGetProduct = function (id) {
        var uri = _config_base_url + '/api/fetch/product';
        var type = 'POST';
        var formdata = {
            id: Base64.encode(id)
        };
        var response = fnAjaxSend(formdata, uri, type, {}, false);
        var data = null;
        if (response.responseJSON.status === 200) {
            data = response.responseJSON.data;
        }
        return data;
    };
    var retrieveGalleries = function () {
        var uri = _config_base_url + '/api/fetch/gallery-list';
        var type = 'POST';
        var formdata = {};
        var response = fnAjaxSend(formdata, uri, type, {}, false);
        var data = null;
        if (response.responseJSON.status === 200) {
            data = response.responseJSON.data;
        }
        return data;
    };
    var fnGetGalleries = function () {
        var data = retrieveGalleries();
        var str = '';
        if (data) {
            for (var i = 0; i < data.length; i++) {
                var link_product = _config_base_url + '/' + _lang + '/product-detail/' + str_slug(data[i].product_details.name) + '/' + data[i].product_item.id;
                var uri = _config_img_url + 'products/item/' + data[i].product_galleries.path_1;
                str = str + '<div class="single_slider d-flex align-items-center" id="carousel" data-bgimg="' + uri + '" alt="">';
                str = str + '    <div class="slider_content">';
                str = str + '       <a class="button" href="' + link_product + '">Buy Now | <b>' + data[i].product_details.name + '</b> (' + data[i].product_item.sku_code + ')</a>';
                str = str + '   </div>';
                str = str + '</div>';
            }
        }
        $('#galleryHomePage').append(str);
    };
    var Ajax = function () {
        return {
            //main function to initiate the module
            init: function () {
                fnToaStr('Ajax js successfully load', 'success', {timeOut: 2000});
                fnSetMenuTop3();
                fnGetCarousel(el_carousel, page_carousel, page_per_show_carousel);
                $(el_carousel).on('afterChange', function (event, slick, currentSlide) {
                    var meta_per_page = $(el_carousel).data('per_page');
                    var meta_total = $(el_carousel).data('total');
                    var meta_total_page = $(el_carousel).data('total_page');
                    if (slick.$slides.length === (currentSlide + 1) && page_carousel <= meta_total_page) {
                        page_carousel++;
                        fnGetCarousel(el_carousel, page_carousel, meta_per_page, 'add');
                    }
                });
                fnGetProductList();
                $('#modal_box').on('shown.bs.modal', function (e) {
                    // do something...
                    var id = $(e.relatedTarget).data('id');
                    var data = fnGetProduct(id);
                    $('h2#mdl_title_detail_product').html(data.detail.name);
                    $('.new_price').html(data.item.price_online);
                    $('.old_price').html(data.item.price_online_masking);
                    $('#mdl_desc_detail_product').html(data.description.description);
                    var str_tab = '';
                    var str_nav = '';
                    if (data.images) {
                        var img = data.images;
                        for (var i = 0; i < img.length; i++) {
                            var _path = img[i].path;
                            var el = '';
                            if (i === 0) {
                                el = ' active';
                            }
                            str_tab = str_tab + '<div class="tab-pane fade show' + el + '" id="tab' + i + '" role="tabpanel"> ';
                            str_tab = str_tab + '   <div class="modal_tab_img"> ';
                            str_tab = str_tab + '       <a href="#"><img src="' + _path + '" alt=""></a> ';
                            str_tab = str_tab + '   </div> ';
                            str_tab = str_tab + '</div> ';

                            str_nav = str_nav + '<li style="width:100px; display:inline-flex"> ';
                            str_nav = str_nav + '   <a class="nav-link' + el + '" data-toggle="tab" href="#tab' + i + '" role="tab" aria-controls="tab1" aria-selected="false"><img src="' + _path + '" alt="" style="width:100%"></a> ';
                            str_nav = str_nav + '</li> ';
                        }
                    }
                    $('.product-details-large').html(str_tab);
                    $('#owl_nav_mdl').html('<ul class="nav product_navactive owl-carousel" role="tablist">' + str_nav + '</ul>');
                });

                fnGetGalleries();
                $('.product_column5').on('changed.owl.carousel initialized.owl.carousel', function (event) {
                    $(event.target).find('.owl-item').removeClass('last').eq(event.item.index + event.page.size - 1).addClass('last')
                }).owlCarousel({
                    autoplay: true,
                    loop: false,
                    nav: true,
                    autoplay: false,
                    autoplayTimeout: 8000,
                    items: 2,
                    margin: 20,
                    dots: false,
                    navText: ['<i class="ion-ios-arrow-thin-left"></i>', '<i class="ion-ios-arrow-thin-right"></i>'],
                    responsiveClass: true,
                    responsive: {
                        0: {
                            items: 1,
                        },
                        576: {
                            items: 2,
                        },
                        768: {
                            items: 3,
                        },
                        992: {
                            items: 4,
                        },
                        1200: {
                            items: 5,
                        },

                    }
                });

                /*---slider activation---*/
                $('.slider_area').owlCarousel({
                    animateOut: 'fadeOut',
                    autoplay: true,
                    loop: true,
                    nav: false,
                    autoplay: false,
                    autoplayTimeout: 8000,
                    items: 1,
                    dots: true,
                });
            }
        };
    }();
    jQuery(document).ready(function () {
        Ajax.init();
    });



</script>