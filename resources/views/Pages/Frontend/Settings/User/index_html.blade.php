<style>
    .modal_tab_button ul li a {
        padding: 0;
        border: 1px solid transparent!important;
        margin: 0 2px;
    }
</style>
<section class="product_area mb-50">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="section_title">
                    <h2><span>@lang('homepage.global.our_product')</span></h2>
                </div>
            </div>
            <div class="col-12">
                <!--<div id="productHomePage" class="product_carousel product_column5 owl-carousel"></div>--> 
                <!-- Set up your HTML -->
                <div id="productHomePage" class="product_carousel product_column5 owl-carousel">
                    
                </div>
            </div>
        </div>
    </div>
</section>