<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//lang = en

return [
    'global' => [
        'our_product' => '<strong>Our</strong> Product',
        '' => '',
    ],
    'categories' => [
        'browse_categories' => 'Browse categories',
        'shopping_now' => 'shopping now'
    ],
    'header' => [
        'my_account' => 'My Account',
        'checkout' => 'Checkout',
        'shopping_cart' => 'Shopping Cart',
        'english' => 'English',
        'indonesian' => 'Indonesian',
    ],
    'footer' => [
        'about_us' => 'About Us',
        'delivery_information' => 'Delivery Information',
        'privacy_policy' => 'Privacy policy',
        'coming_soon' => 'Coming Soon',
        'extra' => 'Extra',
        'term_cond' => 'Terms & Conditions',
        'return' => 'Returns',
        'gift_certificates' => 'Gift Certificates',
        'order_history' => 'Order History',
        'newsletter' => 'Newsletter',
        'newsletter_subscribe' => 'Newsletter Subscribe',
        'we_never_share_your_email' => "We’ll never share your email address with a third-party.",
        'subscribe' => 'Subscribe',
        'copyright' => 'Copyright',
        'information' => 'Information'
    ],
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];
