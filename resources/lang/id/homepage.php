<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//lang = id

return [
    'global' => [
        'our_product' => 'Produk <strong>kami</strong>',
        '' => '',
    ],
    'categories' => [
        'browse_categories' => 'cari kategori',
        'shopping_now' => 'belanja sekarang'
    ],
    'header' => [
        'my_account' => 'Akun ku',
        'checkout' => 'Selesaikan belanja',
        'shopping_cart' => 'Keranjang Belanja',
        'english' => 'Inggris',
        'indonesian' => 'Indonesia',
    ],
    'footer' => [
        'about_us' => 'Tentang kami',
        'delivery_information' => 'Informasi pengantaran barang',
        'privacy_policy' => 'Ketentuan privasi',
        'coming_soon' => 'Akan datang',
        'extra' => 'Ekstra',
        'term_cond' => 'Syarat & Ketentuan',
        'return' => 'Kembali',
        'gift_certificates' => 'Sertifikat hadiah',
        'order_history' => 'Riwayat Pemesanan',
        'newsletter' => 'Langganan Berita',
        'newsletter_subscribe' => 'Berlangganan Berita',
        'we_never_share_your_email' => "Kami tidak akan membagikan alamat email anda kepada pihak ketiga.",
        'subscribe' => 'Langganan',
        'copyright' => 'Hak Cipta'
    ],
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
    '' => '',
];
